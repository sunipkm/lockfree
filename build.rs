extern crate rustc_version;
use rustc_version::{version, Version};


fn main() {
    // Assert we haven't travelled back in time
    assert!(version().unwrap().major >= 1);

    // Check for a deprecation of [`std::mem::uninitialized`]
    if version().unwrap() >= Version::parse("1.39.0").unwrap() {
        println!("cargo:rustc-cfg=no_mem_uninit");
    }
    else {
        println!("cargo:rustc-cfg=mem_uninit");
    }
    // Check for a deprecation of [`std::atomic::compare_and_swap`]
    if version().unwrap() >= Version::parse("1.50.0").unwrap() {
        println!("cargo:rustc-cfg=no_cmpswap");
    }
    else {
        println!("cargo:rustc-cfg=cmpswap");
    }
}